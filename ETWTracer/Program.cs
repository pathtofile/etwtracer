﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Session;

namespace ETWTracer
{
    class Program
    {
        static void parseData(TraceEvent data)
        {
            Console.WriteLine(data.TaskName);
            foreach (string payloadname in data.PayloadNames)
            {
                var payload = data.PayloadByName(payloadname);
                string outout = "\t" + payloadname + " <" + payload.GetType() + "> | " + payload;
                Console.WriteLine(outout);
            }
            Console.WriteLine("---------------------");
        }

        static void Main(string[] args)
        {
            // Today you have to be Admin to turn on ETW events (anyone can write ETW events).   
            if (!(TraceEventSession.IsElevated() ?? false))
            {
                Console.WriteLine("Run as admin");
                return;
            }
            string sessionName = "etwtracer";

            // This is the name of the event source.   
            using (TraceEventSession traceEventSession = new TraceEventSession(sessionName, null))
            {
                traceEventSession.StopOnDispose = true;
                Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
                {
                    traceEventSession.Dispose();
                };

                string providerName = "Microsoft-Windows-Kernel-Process";
                ulong providerKeywords = 0x10;

                if (args.Length > 0)
                {
                    // Get provider name from commandline args
                    providerName = args[0];
                    if (args.Length > 1)
                    {
                        string sKeywords = args[1];
                        if (sKeywords.StartsWith("0x"))
                        {
                            providerKeywords = Convert.ToUInt64(sKeywords.Substring(2), 16);
                        }
                        else
                        {
                            providerKeywords = Convert.ToUInt64(sKeywords);
                        }
                    }
                }

                Guid providerGuid = TraceEventProviders.GetProviderGuidByName(providerName);
                traceEventSession.EnableProvider(providerGuid, TraceEventLevel.Verbose, providerKeywords);

                ETWTraceEventSource source = traceEventSession.Source;
                source.Dynamic.All += parseData;
                Console.WriteLine(String.Format("Starting ETW Log of {0} <{1}> Keywords: 0x{2:x2} ...", providerName, providerGuid, providerKeywords));
                source.Process();
            }
            Console.WriteLine("Shutting Down...");
        }
    }
}
